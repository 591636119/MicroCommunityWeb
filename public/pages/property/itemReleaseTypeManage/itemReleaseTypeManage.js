/**
    入驻小区
**/
(function (vc) {
    var DEFAULT_PAGE = 1;
    var DEFAULT_ROWS = 10;
    vc.extends({
        data: {
            itemReleaseTypeManageInfo: {
                itemReleaseTypes: [],
                total: 0,
                records: 1,
                moreCondition: false,
                typeId: '',
                conditions: {
                    typeId: '',
                    typeName: '',
                    communityId: vc.getCurrentCommunity().communityId,
                }
            }
        },
        _initMethod: function () {
            vc.component._listItemReleaseTypes(DEFAULT_PAGE, DEFAULT_ROWS);
        },
        _initEvent: function () {
            vc.on('itemReleaseTypeManage', 'listItemReleaseType', function (_param) {
                vc.component._listItemReleaseTypes(DEFAULT_PAGE, DEFAULT_ROWS);
            });
            vc.on('pagination', 'page_event', function (_currentPage) {
                vc.component._listItemReleaseTypes(_currentPage, DEFAULT_ROWS);
            });
        },
        methods: {
            _listItemReleaseTypes: function (_page, _rows) {
                vc.component.itemReleaseTypeManageInfo.conditions.page = _page;
                vc.component.itemReleaseTypeManageInfo.conditions.row = _rows;
                let param = {
                    params: vc.component.itemReleaseTypeManageInfo.conditions
                };
                //发送get请求
                vc.http.apiGet('/itemRelease.listItemReleaseType',
                    param,
                    function (json, res) {
                        var _itemReleaseTypeManageInfo = JSON.parse(json);
                        vc.component.itemReleaseTypeManageInfo.total = _itemReleaseTypeManageInfo.total;
                        vc.component.itemReleaseTypeManageInfo.records = _itemReleaseTypeManageInfo.records;
                        vc.component.itemReleaseTypeManageInfo.itemReleaseTypes = _itemReleaseTypeManageInfo.data;
                        vc.emit('pagination', 'init', {
                            total: vc.component.itemReleaseTypeManageInfo.records,
                            currentPage: _page
                        });
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            },
            _openAddItemReleaseTypeModal: function () {
                vc.emit('addItemReleaseType', 'openAddItemReleaseTypeModal', {});
            },
            _openEditItemReleaseTypeModel: function (_itemReleaseType) {
                vc.emit('editItemReleaseType', 'openEditItemReleaseTypeModal', _itemReleaseType);
            },
            _openDeleteItemReleaseTypeModel: function (_itemReleaseType) {
                vc.emit('deleteItemReleaseType', 'openDeleteItemReleaseTypeModal', _itemReleaseType);
            },
            _queryItemReleaseTypeMethod: function () {
                vc.component._listItemReleaseTypes(DEFAULT_PAGE, DEFAULT_ROWS);

            },
            _moreCondition: function () {
                if (vc.component.itemReleaseTypeManageInfo.moreCondition) {
                    vc.component.itemReleaseTypeManageInfo.moreCondition = false;
                } else {
                    vc.component.itemReleaseTypeManageInfo.moreCondition = true;
                }
            },
            _settingFlow:function(itemReleaseType){
                window.open('/bpmnjs/index.html?flowId=' + itemReleaseType.flowId + "&modelId=" + itemReleaseType.modelId);
            },

            _openDeployWorkflow: function (itemReleaseType) {
                let _param = {
                    modelId: itemReleaseType.modelId
                };
                //发送get请求
                vc.http.apiPost('/workflow/deployModel',
                    JSON.stringify(_param),
                    {
                        emulateJSON: true
                    },
                    function (json, res) {
                        let _json = JSON.parse(json);
                        vc.toast(_json.msg)
                        vc.emit('itemReleaseTypeManage', 'listItemReleaseType', {});
                    }, function (errInfo, error) {
                        console.log('请求失败处理');
                    }
                );
            }


        }
    });
})(window.vc);
